﻿using System.Diagnostics;
using Xamarin.Forms;

namespace Calculator
{
    public partial class CalculatorPage : ContentPage
    {
        private double first = 0;
        private char oper;
        public CalculatorPage()
        {
            InitializeComponent();
        }

        private void UpdateLabels()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(UpdateLabels)}");
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Button button = sender as Button;
            if (button.Text.Equals("=")) {
                Result();
            } else if (DataLabel.Text.Equals(("C"))) {
                first = 0;
                DataLabel.Text = "0";
            } else if (!isNumber(button.Text)) {
                setOperator(button.Text[0]);
            } else if (DataLabel.Text.Equals("0")) {
                DataLabel.Text = button.Text;
            } else {
                DataLabel.Text += button.Text;
            }
        }

        bool isNumber(string data) {
            int res;
            if (int.TryParse(data, out res)) {
                return true;   
            }
            return false;
        }

        void setOperator(char op){
            oper = op;
            first = double.Parse(DataLabel.Text);
            DataLabel.Text = "0";
        }

        void Result() {
            if (oper.Equals('+')) {
                DataLabel.Text = (first + double.Parse(DataLabel.Text)).ToString();
                first = 0;
                oper = 'z';
            } else if (oper.Equals('-')) {
                DataLabel.Text = (first - double.Parse(DataLabel.Text)).ToString();
                first = 0;
                oper = 'z';
            } else if (oper.Equals('x')) {
                DataLabel.Text = (first * double.Parse(DataLabel.Text)).ToString();
                first = 0;
                oper = 'z';
            } else if (oper.Equals('/') && !DataLabel.Text.Equals("0")) {
                DataLabel.Text = (first / double.Parse(DataLabel.Text)).ToString();
                first = 0;
                oper = 'z';
            }
        }
    }
}
